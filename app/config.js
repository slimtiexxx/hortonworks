
function config($routeProvider, $locationProvider) {

    // routes
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);

}

// Config Injections
config.$inject = ['$routeProvider', '$locationProvider'];

module.exports = config;
function MainController($scope, $http){

    $scope.search = function(e){
        e.preventDefault();

        $scope.noResults = false;
        $scope.loading = true;
        $scope.data = {};

        $http({
            method: 'GET',
            url: 'https://api.github.com/search/repositories',
            params: {
                q: $scope.repoName
            }
        }).then(function success(response) {

            if (response.data.total_count !== 0) {
                $scope.data = response.data;

                for (var i = 0; i < $scope.data.items.length; i++) {
                    $scope.data.items[i].hide_issues = true;
                }

                $scope.loading = false;

            } else {

                $scope.noResults = true;
                $scope.loading = false;
            }

        }, function error() {
            $scope.loading = false;
        });

    };

    $scope.moreDetails = function (repo) {

        if (!repo.issues) {

            $http({
                method: 'GET',
                url: 'https://api.github.com/search/issues?q=repo:' + repo.full_name
            }).then(function success(response) {
                repo.issues = response.data.items;
                repo.hide_issues = !repo.hide_issues;

            }, function error(response) {

            });
        } else {
            repo.hide_issues = !repo.hide_issues;
        }
    }
}

MainController.$inject = ['$scope', '$http'];

module.exports = MainController;
var angular = require('angular');

// Import Config
var config = require('./config');

// Import Controllers
var MainController = require('./main-controller');

// Import Directives
var headerDirective = require('../components/header.directive.js');
var footerDirective = require('../components/footer.directive.js');

// Import Dependencies
require('angular-route');


angular
    .module('hortonworks', [
        'ngRoute'
    ])

    // Config inject
    .config(config)

    // Controllers inject
    .controller('MainController', MainController)

    // Directives inject
    .directive('headerDirective', headerDirective)
    .directive('footerDirective', footerDirective);

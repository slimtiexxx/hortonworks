# Hortonworks Angular JS Demo
##### Simple AngularJS project with SASS support and Gulp serve/build tasks using Bootstrap 4 framework grid system
#### [Demo: andrewsite.webuda.com/hortonworks/](http://andrewsite.webuda.com/hortonworks/)

# How to use it

## 1. Download
```bash
git clone https://gitlab.com/slimtiexxx/hortonworks.git
```

## 2. Setup
```bash
npm install
```

* this will install all the required dependencies

## 3. Build js
```bash
gulp scripts
```
## 4. Build css
```bash
gulp sass
```

## Or all in together run building process
```bash
gulp serve
```

this will process following tasks:
* start a webserver using Browserify live reload
* compile SCSS files to compiled css
* merge all JS files and copy

(all SCSS/HTML will be watched for changes and injected into browser thanks to BrowserSync)